module Trees where

data Tree a = EmptyNode
    | FullNode a (Tree a) (Tree a) 
    | Borked deriving (Show, Eq)

treeInsert :: (Ord int) => (Tree int) -> int -> (Tree int)
treeInsert EmptyNode a = FullNode a EmptyNode EmptyNode
treeInsert (FullNode a t1 t2) b | b < a = FullNode a (treeInsert t1 b) t2
                                | b > a = FullNode a t1 (treeInsert t2 b)
                                | b == a = FullNode a t1 t2

findAncestor :: (Ord int) => (Tree int) -> int -> int -> int
findAncestor EmptyNode _ _ = undefined
findAncestor (FullNode a t1 t2) b c | not (treeContains (FullNode a t1 t2) b && treeContains (FullNode a t1 t2) c) = undefined
                                    | b < a && c < a = findAncestor t1 b c
                                    |b > a && c > a = findAncestor t2 b c
                                    | otherwise = a
    
treeContains :: (Ord int) => (Tree int) -> int -> Bool
treeContains EmptyNode _ = False
treeContains (FullNode a t1 t2) b | b == a = True 
                                  | b > a = treeContains t2 b
                                  | b < a = treeContains t1 b