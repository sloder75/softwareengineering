module Main where

    import Control.Exception
    import Test.HUnit 
    import Test.Framework as TF (defaultMain, testGroup, Test) 
    import Test.Framework.Providers.HUnit (testCase)
    import Test.Framework.Providers.QuickCheck2 (testProperty)  
    import Trees
    
    {- HUnit Tests -}
    

    myTree = EmptyNode
    myTree2 = (FullNode 5 EmptyNode EmptyNode) 
    myTree3 = (FullNode 5 (FullNode 4 EmptyNode EmptyNode) EmptyNode)
    myTree4 = (FullNode 5 (FullNode 4 (FullNode 2 EmptyNode EmptyNode) EmptyNode) EmptyNode)
    myTree5 = (FullNode 5 (FullNode 4 (FullNode 2 EmptyNode (FullNode 3 EmptyNode EmptyNode)) EmptyNode) EmptyNode)
    myTree6 = (FullNode 5 (FullNode 4 (FullNode 2 (FullNode 1 EmptyNode EmptyNode) (FullNode 3 EmptyNode EmptyNode)) EmptyNode) EmptyNode)
    myTree7 = (FullNode 5 (FullNode 4 (FullNode 2 (FullNode 1 EmptyNode EmptyNode) (FullNode 3 EmptyNode EmptyNode)) EmptyNode) (FullNode 6 EmptyNode EmptyNode)) 





    test_insert  = (treeInsert myTree 5) @?= (FullNode 5 EmptyNode EmptyNode) 
    test_insert2 = (treeInsert myTree2 4) @?= (FullNode 5 (FullNode 4 EmptyNode EmptyNode) EmptyNode) 
    test_insert3 = (treeInsert myTree3 2) @?= (FullNode 5 (FullNode 4 (FullNode 2 EmptyNode EmptyNode) EmptyNode) EmptyNode) 
    test_insert4 = (treeInsert myTree4 3) @?= (FullNode 5 (FullNode 4 (FullNode 2 EmptyNode (FullNode 3 EmptyNode EmptyNode)) EmptyNode) EmptyNode) 
    test_insert5 = (treeInsert myTree5 1) @?= (FullNode 5 (FullNode 4 (FullNode 2 (FullNode 1 EmptyNode EmptyNode) (FullNode 3 EmptyNode EmptyNode)) EmptyNode) EmptyNode) 
    test_insert6 = (treeInsert myTree6 6) @?= (FullNode 5 (FullNode 4 (FullNode 2 (FullNode 1 EmptyNode EmptyNode) (FullNode 3 EmptyNode EmptyNode)) EmptyNode) (FullNode 6 EmptyNode EmptyNode)) 
    test_insert7 = (treeInsert myTree7 6) @?= (FullNode 5 (FullNode 4 (FullNode 2 (FullNode 1 EmptyNode EmptyNode) (FullNode 3 EmptyNode EmptyNode)) EmptyNode) (FullNode 6 EmptyNode EmptyNode))
    
    {- 
              5
             / \
            4   6
           / \ / \
          2  
         / \
        1   3
       / \ / \
    -}
    
    test_ancestor  = do x <- try (evaluate (findAncestor myTree7 8 9)) :: IO (Either ErrorCall Integer)
                        case x of
                            Right cor -> assertFailure ""
                            Left wrn -> putStr ""
    test_ancestor2 = do x <- try (evaluate (findAncestor myTree7 9 4)) :: IO (Either ErrorCall Integer)
                        case x of
                            Right cor -> assertFailure ""
                            Left wrn -> putStr ""
    test_ancestor3 = do x <- try (evaluate (findAncestor myTree7 1 9)) :: IO (Either ErrorCall Integer)
                        case x of
                            Right cor -> assertFailure ""
                            Left wrn -> putStr "" 
    test_ancestor4 = (findAncestor myTree7 1 4) @?= 4 
    test_ancestor5 = (findAncestor myTree7 1 3) @?= 2 
    test_ancestor6 = (findAncestor myTree7 3 6) @?= 5 
    test_ancestor7 = (findAncestor myTree7 3 3) @?= 3 
    
    
    {- QuickCheck Tests -}
    
    
    main = defaultMain tests

    tests :: [TF.Test]
    tests = [testGroup "first test group"[  testCase "Inserting 5" test_insert,
                                            testCase "Inserting 4" test_insert2,
                                            testCase "Inserting 2" test_insert3,
                                            testCase "Inserting 3" test_insert4,
                                            testCase "Inserting 1" test_insert5,
                                            testCase "Inserting 6" test_insert6,
                                            testCase "Inserting 6" test_insert7,
                                            testCase "Searching the ancestor of 8 and 9" test_ancestor,
                                            testCase "Searching the ancestor of 9 and 4" test_ancestor2,
                                            testCase "Searching the ancestor of 1 and 9" test_ancestor3,
                                            testCase "Searching the ancestor of 1 and 4" test_ancestor4,
                                            testCase "Searching the ancestor of 1 and 3" test_ancestor5,
                                            testCase "Searching the ancestor of 3 and 6" test_ancestor6,
                                            testCase "Searching the ancestor of 3 and 3" test_ancestor7
                                         ]
            ]
    